package com.reto.servicionumerodos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.reto.servicionumerodos.model.CalcularImcRequest;
import com.reto.servicionumerodos.model.CalcularNumerosRequest;
import com.reto.servicionumerodos.model.CalcularNumerosResponse;
import com.reto.servicionumerodos.model.ImcResponse;
import com.reto.servicionumerodos.service.ServicioNumeroDosService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;

/**
 * The class servicioNumeroDosController.
 * 
 * @author Alex
 *
 *
 */
@RequestMapping("${api.basepath}")
@RestController
@AllArgsConstructor
public class ServicioNumeroDosController {
	
	/** The service for numero uno service. */
	@Autowired
	private ServicioNumeroDosService service;
	
	/**
	 * Method to set greeting.
	 * 
	 * @param request The SetGreetingRequest object
	 * @throws Exception
	 */
	@ApiOperation(value = "Calcular IMC")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Registry successfull"),
			@ApiResponse(code = 400, message = "Bad Request") })
	@PostMapping(value = "${api.calularimc}")
	public ResponseEntity<ImcResponse> calcularImc(@RequestBody CalcularImcRequest request) throws Exception {
		ImcResponse response = service.calcularImcServ(request);
		return new ResponseEntity<>(response,HttpStatus.OK);
	}
	
	/**
	 * Method to set greeting.
	 * 
	 * @param request The SetGreetingRequest object
	 * @throws Exception
	 */
	@ApiOperation(value = "Calcular IMC")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Registry successfull"),
			@ApiResponse(code = 400, message = "Bad Request") })
	@PostMapping(value = "${api.calcularnumeros}")
	public ResponseEntity<CalcularNumerosResponse> calcularNums(@RequestBody CalcularNumerosRequest request) throws Exception {
		CalcularNumerosResponse response = service.calculaNumerosService(request);
		return new ResponseEntity<>(response,HttpStatus.OK);
	}

}
