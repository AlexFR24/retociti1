package com.reto.servicionumerodos.exception;

/**
 * The enum ErrorType.
 * 
 * @author AdsWare System
 *
 */
public enum ErrorType {

	ERROR, INVALID, FATAL
	
}
