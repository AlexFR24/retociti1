package com.reto.servicionumerodos.constant;

import lombok.Data;
import org.springframework.stereotype.*;
import org.springframework.beans.factory.annotation.Value;

@Component
@Data
public class Constants {
	
	public final static Double zeroDouble = 0.0;
	
	public final static Integer zeroInt = 0;
	
	public final static Integer oneInt = 1;
	
	public static Integer threeInt = 3;
	
	public static String errorInvalidType = "Tipo de usuario incorrecto.";
	
	public static String errorNotUser = "Usuario no registrado.";
	
	public static String endpointSet = "set/saludo/gen";
	
	public static String endpointFind = "find/greeting";

	/** The SPACE constant. */
	public static final String SPACE = " ";

	/** The EMPTY_STRING constant. */
	public static final String EMPTY_STRING = "";

	/** The ERR_TIME_PATTERN constant. */
	public static final String ERR_TIME_PATTERN = "yyyy-MM-dd'T'HH:mm:ssZ";

	/** The COMMA_SEPARATOR constant. */
	public static final String COMMA_SEPARATOR = ",";

	/** The ERROR_RESPONSE_TYPE constant. */
	public static final String ERROR_RESPONSE_TYPE = "errorResponse.type, {}";

	/** The ERROR_RESPONSE_CODE constant. */
	public static final String ERROR_RESPONSE_CODE = "errorResponse.code, {}";

	/** The ERROR_RESPONSE_DETAILS constant. */
	public static final String ERROR_RESPONSE_DETAILS = "errorResponse.details, {}";

	/** The ERROR_RESPONSE_LOCATION constant. */
	public static final String ERROR_RESPONSE_LOCATION = "errorResponse.location, {}";

	/** The ERROR_RESPONSE_MORE_INFO constant. */
	public static final String ERROR_RESPONSE_MORE_INFO = "errorResponse.moreinfo, {}";

	/** The ERROR_CODE_BAD_REQUEST constant. */
	public static final String ERROR_CODE_BAD_REQUEST = "400";

}
