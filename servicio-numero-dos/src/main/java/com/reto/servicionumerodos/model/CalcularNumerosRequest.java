package com.reto.servicionumerodos.model;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;

/**
 * The class findGreetingRequest.
 * 
 * @author Alex
 *
 */
@Getter
@Setter
@ApiModel(description = "CalcularNumerosRequest")
public class CalcularNumerosRequest {
	
	/** The numbers string. */
	@NotNull
	String numeros;
}
