package com.reto.servicionumerodos.model;

import java.util.ArrayList;
import java.util.LinkedList;

import com.reto.servicionumerodos.constant.Constants;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;

/**
 * The class CalcularNumerosResponse.
 * 
 * @author Alex
 *
 */
@Getter
@Setter
@ApiModel(description = "NumerosResponse")
public class CalcularNumerosResponse {
	LinkedList<Integer> mayorMenor;
	LinkedList<Integer> menorMayor;
	Integer numMasRepetido;
	Double prom;
	Integer maxRepeticiones = Constants.zeroInt;
	
	public void mayorMenor(LinkedList<Integer> array) {
		this.mayorMenor.addAll(array);
	}
	
	public void menorMayor(LinkedList<Integer> array) {
		this.menorMayor.addAll(array);
	}
}
