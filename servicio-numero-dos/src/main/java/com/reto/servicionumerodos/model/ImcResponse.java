package com.reto.servicionumerodos.model;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;

/**
 * The class FindGreetingResponse.
 * 
 * @author Alex
 *
 */
@Getter
@Setter
@ApiModel(description = "Imc")
public class ImcResponse {
	
	/** The imc attribute. */
	private Double imc;
}
