package com.reto.servicionumerodos.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.reto.servicionumerodos.constant.Constants;
import com.reto.servicionumerodos.feign.ServicioUnoFeign;
import com.reto.servicionumerodos.model.CalcularImcRequest;
import com.reto.servicionumerodos.model.CalcularNumerosRequest;
import com.reto.servicionumerodos.model.CalcularNumerosResponse;
import com.reto.servicionumerodos.model.ImcResponse;
import com.reto.servicionumerodos.model.User;
import org.springframework.beans.factory.annotation.Autowired;

import lombok.AllArgsConstructor;

/**
 * The class ServicioNumeroDosService.
 * 
 * @author Alex
 *
 */
@Service
@AllArgsConstructor
public class ServicioNumeroDosService {
	
	@Autowired
	private ServicioUnoFeign feign;
	
	 /**
	 * Method to calculate IMC.
	 * 
	 * @param request The CalcularImcRequest object
	 */
	public ImcResponse calcularImcServ(CalcularImcRequest request) {
		User user = feign.getUser(request).getBody();
		ImcResponse response = new ImcResponse();
		response.setImc(user.getWeight() / (user.getHeight() * user.getHeight()));
		return response;
	}
	
	 /**
	 * Method to calculate IMC.
	 * 
	 * @param request The CalcularImcRequest object
	 */
	public CalcularNumerosResponse calculaNumerosService(CalcularNumerosRequest request) {
		CalcularNumerosResponse response = new CalcularNumerosResponse();
		List<String> numerosString = Arrays.asList(request.getNumeros().split(Constants.COMMA_SEPARATOR));
		LinkedList<Integer> mayorMenor = new LinkedList<Integer>();
		LinkedList<Integer> menorMayor = new LinkedList<Integer>();
		
		numerosString.forEach((s) -> {
			int numInt = Integer.parseInt(s);
			if (mayorMenor.isEmpty()) {
				mayorMenor.add(numInt);
			}else if(mayorMenor.getLast() >= numInt) {
				mayorMenor.addLast(numInt);
			}else if(mayorMenor.getFirst() <= numInt) {
				mayorMenor.addFirst(numInt);
			}else if(mayorMenor.getLast() < numInt && mayorMenor.getFirst() > numInt) {
				
				for(int i = Constants.zeroInt; i < mayorMenor.size(); i++) {
					if(numInt >= mayorMenor.get(i)) {
						mayorMenor.add(i, numInt);
						break;
					}
				}
			}
				
		});
		
		
		
		
		Map<Integer,Integer> mapProm = new HashMap<Integer,Integer>();
		mayorMenor.forEach((s) -> {
			menorMayor.addFirst(s);
			if(!mapProm.containsKey(s)) {
				mapProm.put(s, Constants.oneInt);
			}else {
				mapProm.put(s, mapProm.get(s) + Constants.oneInt);
			}
		});
		response.setMaxRepeticiones(Constants.zeroInt);
		response.setProm(Constants.zeroDouble);
		mapProm.forEach((k,p) -> {
			if(p > response.getMaxRepeticiones()) {
				response.setMaxRepeticiones(p);
				response.setNumMasRepetido(k);
			}
		});
		
		if(!(response.getMaxRepeticiones() == Constants.zeroInt))
			response.setProm(response.getProm() / mapProm.size());
		
		response.setMayorMenor(mayorMenor); 
		response.setMenorMayor(menorMayor);
		
		return response;
	}
}
