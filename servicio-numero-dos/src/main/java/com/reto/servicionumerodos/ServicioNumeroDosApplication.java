package com.reto.servicionumerodos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan({"com.*"})
@EnableFeignClients(basePackages = "com.*")
@SpringBootApplication
public class ServicioNumeroDosApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServicioNumeroDosApplication.class, args);
	}

}
