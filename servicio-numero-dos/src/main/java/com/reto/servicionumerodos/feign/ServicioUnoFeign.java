package com.reto.servicionumerodos.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.reto.servicionumerodos.model.CalcularImcRequest;
import com.reto.servicionumerodos.model.User;

import org.springframework.http.ResponseEntity;

@FeignClient(name = "${api.serviciouno}")
public interface ServicioUnoFeign {
	
	@PostMapping(value = "${api.finduser}")
	ResponseEntity<User> getUser(@RequestBody CalcularImcRequest userRequest);

}
