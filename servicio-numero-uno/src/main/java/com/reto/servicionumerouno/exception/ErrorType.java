package com.reto.servicionumerouno.exception;

/**
 * The enum ErrorType.
 * 
 * @author AdsWare System
 *
 */
public enum ErrorType {

	ERROR, INVALID, FATAL
	
}
