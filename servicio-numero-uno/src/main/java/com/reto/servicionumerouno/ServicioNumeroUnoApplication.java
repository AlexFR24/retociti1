package com.reto.servicionumerouno;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServicioNumeroUnoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServicioNumeroUnoApplication.class, args);
	}

}
