package com.reto.servicionumerouno.model;

import java.util.ArrayList;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;

/**
 * The class SetGreetingRequest.
 * 
 * @author Alex
 *
 */
@Getter
@Setter
@ApiModel(description = "Set user request")
public class SetUserRequest {
	
	private ArrayList<User> users;
	
}
