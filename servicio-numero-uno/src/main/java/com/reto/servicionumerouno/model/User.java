package com.reto.servicionumerouno.model;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;

/**
 * The class User.
 * 
 * @author Alex
 *
 */
@Getter
@Setter
@ApiModel(description = "User")
public class User {
	
	/** The user name attribute. */
	@NotNull
	private String name;
	
	/** The user last name attribute. */
	@NotNull
	private String lastName;
	
	/** The user age attribute. */
	@NotNull
	private Integer age;
	
	/** The user gender attribute. */
	@NotNull
	private enum gender{
		HOMBRE,
		MUJER
	}
	
	/** The user nationality attribute. */
	@NotNull
	private String nationality;
	
	/** The user height attribute. */
	@NotNull
	private Double height;
	
	/** The user weight attribute. */
	@NotNull
	private Double weight;
	
	/** The user type attribute. */
	@NotNull
	private Integer userType;
}
