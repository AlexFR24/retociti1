package com.reto.servicionumerouno.model;

import java.util.HashMap;
import java.util.Map;

import lombok.Data;
import org.springframework.stereotype.Component;

@Component
@Data
public class Greeting {
	
	Map<Integer,String> greeting = new HashMap<Integer,String>();

}
