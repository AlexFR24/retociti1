package com.reto.servicionumerouno.model;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;

/**
 * The class SetGreetingRequest.
 * 
 * @author Alex
 *
 */
@Getter
@Setter
@ApiModel(description = "Set greeting request")
public class SetGreetingRequest {
	/** The UserType attribute. */
	@NotNull
	private Integer userType;
	
	/** The greeting attribute. */
	@NotNull
	private String greeting;
}
