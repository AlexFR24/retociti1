package com.reto.servicionumerouno.model;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;

/**
 * The class FindGreetingResponse.
 * 
 * @author Alex
 *
 */
@Getter
@Setter
@ApiModel(description = "FindGreeting")
public class FindGreetingResponse {
	
	/** The greeting attribute. */
	private String greeting;
}
