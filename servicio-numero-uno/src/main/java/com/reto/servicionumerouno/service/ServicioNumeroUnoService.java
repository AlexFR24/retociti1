package com.reto.servicionumerouno.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.reto.servicionumerouno.model.Greeting;
import com.reto.servicionumerouno.model.SetGreetingRequest;
import com.reto.servicionumerouno.model.SetUserRequest;
import com.reto.servicionumerouno.model.User;
import com.reto.servicionumerouno.model.FindGreetingRequest;
import com.reto.servicionumerouno.model.FindGreetingResponse;
import com.reto.servicionumerouno.constants.Constants;
import com.reto.servicionumerouno.exception.BadRequestException;
import lombok.AllArgsConstructor;

/**
 * The class ServicioNumeroUnoService.
 * 
 * @author Alex
 *
 */
@Service
@AllArgsConstructor
public class ServicioNumeroUnoService {
	
	@Value("${defaultgreeting}")
	public static String defaultGreeting;
	
	 /**
	 * Method to set greeting.
	 * 
	 * @param request The SetGreetingRequest object
	 * @param request the Greeting object
	 */
	public void setGreetingService(SetGreetingRequest request, Greeting greetings) {
		if(request.getUserType() < Constants.zeroInt || request.getUserType() > Constants.threeInt) {
			throw new BadRequestException(Constants.errorInvalidType, Constants.endpointSet);
		}
		greetings.getGreeting().put(request.getUserType(), request.getGreeting());
	}
	
	 /**
	 * Method to set new user.
	 * @param SetUserRequest
	 * @param Map<String,User>
	 */
	public void setUserService(SetUserRequest request, Map<String,User> usersMap) {
		request.getUsers().forEach((u) -> {
			usersMap.put(u.getName().toUpperCase().concat(Constants.SPACE).concat(u.getLastName().toUpperCase()), u);
		});
	}
	
	 /**
	 * Method to set new user.
	 * @param SetUserRequest
	 * @param Map<String,User>
	 */
	public FindGreetingResponse findUserGreetingService(FindGreetingRequest request, Map<String,User> usersMap, Greeting greetings) {
		FindGreetingResponse response = new FindGreetingResponse();
		Integer userType = Constants.zeroInt;
		
		String key = request.getName().toUpperCase().concat(Constants.SPACE).concat(request.getLastName().toUpperCase());
		
		if(!usersMap.containsKey(key)) {
			throw new BadRequestException(Constants.errorNotUser, Constants.endpointFind);
		}
		userType = usersMap.get(key).getUserType();
		
		if(!greetings.getGreeting().containsKey(userType)) {
			userType = Constants.zeroInt;
		}
		response.setGreeting(greetings.getGreeting().getOrDefault(userType, Constants.DEFAULT_GREETING));
		
		return response;
	}
	
	 /**
	 * Method to find and get a user.
	 * 
	 * @param request The FindGreetingRequest object
	 * @param Map<String,User>
	 */
	public User findUserService(FindGreetingRequest request,  Map<String,User> usersMap) {
		String key = request.getName().toUpperCase().concat(Constants.SPACE).concat(request.getLastName().toUpperCase());
		if(!usersMap.containsKey(key)) {
			throw new BadRequestException(Constants.errorNotUser, Constants.endpointFind);
		}
		return usersMap.get(key);
	}
	
}
