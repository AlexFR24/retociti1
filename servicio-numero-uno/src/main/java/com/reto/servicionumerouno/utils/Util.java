package com.reto.servicionumerouno.utils;

import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The class Util.
 * 
 * @author AdsWare System
 *
 */
public class Util {
	
  /**
   * The log attribute.
   */
  private static final Logger log = LoggerFactory.getLogger(Util.class);

  /**
   * Builder.
   */
  private Util() {
	  
  }
  
  /**
   * Method to define default value.
   * 
   * @param target the target
   * @param defaultValue the defaultValue
   * @return the object description
   */
  public static Object defaultValue(Object target, Object defaultValue) {
    return Objects.isNull(target) ? defaultValue : target;
  }
    

}
