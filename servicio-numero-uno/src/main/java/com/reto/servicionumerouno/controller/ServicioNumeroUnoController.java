package com.reto.servicionumerouno.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.reto.servicionumerouno.model.FindGreetingRequest;
import com.reto.servicionumerouno.model.FindGreetingResponse;
import com.reto.servicionumerouno.model.Greeting;
import com.reto.servicionumerouno.model.SetGreetingRequest;
import com.reto.servicionumerouno.model.SetUserRequest;
import com.reto.servicionumerouno.model.User;
import com.reto.servicionumerouno.service.ServicioNumeroUnoService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;

/**
 * The class servicioNumeroUnoController.
 * 
 * @author Alex
 *
 *
 */
@RequestMapping("${api.basepath}")
@RestController
@AllArgsConstructor
public class ServicioNumeroUnoController {

	/** The service for numero uno service. */
	@Autowired
	private ServicioNumeroUnoService setRequest;
	
	private Greeting greetings = new Greeting();
	
	private Map<String,User> usersMap = new HashMap<String,User>();

	/**
	 * Method to set greeting.
	 * 
	 * @param request The SetGreetingRequest object
	 * @throws Exception
	 */
	@ApiOperation(value = "Set greeting")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Registry successfull"),
			@ApiResponse(code = 400, message = "Bad Request") })
	@PostMapping(value = "${api.setgreeting}")
	public ResponseEntity<Void> setGreeting(@RequestBody SetGreetingRequest request) throws Exception {
		setRequest.setGreetingService(request, greetings);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	/**
	 * Method to set user.
	 * 
	 * @param request The SetUserRequest object
	 * @throws Exception
	 */
	@ApiOperation(value = "Set user")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Registry successfull"),
			@ApiResponse(code = 400, message = "Bad Request") })
	@PostMapping(value = "${api.setuser}")
	public ResponseEntity<Void> setUser(@RequestBody SetUserRequest request) throws Exception {
		setRequest.setUserService(request, usersMap);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	/**
	 * Method to set user.
	 * 
	 * @param request The SetUserRequest object
	 * @throws Exception
	 */
	@ApiOperation(value = "Set user")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Registry successfull"),
			@ApiResponse(code = 400, message = "Bad Request") })
	@PostMapping(value = "${api.findgreeting}")
	public ResponseEntity<FindGreetingResponse> findGreeting(@RequestBody FindGreetingRequest request) throws Exception {
		FindGreetingResponse response = setRequest.findUserGreetingService(request, usersMap, greetings);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	/**
	 * Method to get user.
	 * 
	 * @param request The SetUserRequest object
	 * @throws Exception
	 */
	@ApiOperation(value = "Set user")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Registry successfull"),
			@ApiResponse(code = 400, message = "Bad Request") })
	@PostMapping(value = "${api.finduser}")
	public ResponseEntity<User> findUser(@RequestBody FindGreetingRequest request) throws Exception {
		User response = setRequest.findUserService(request, usersMap);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

}
